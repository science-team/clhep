Source: clhep
Section: science
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Stephan Lachnit <stephanlachnit@debian.org>
Build-Depends: debhelper-compat (= 13),
               dpkg-dev (>= 1.22.5),
               d-shlibs,
               dh-exec,
               cmake,
               ninja-build,
               doxygen,
               texlive-latex-base,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://proj-clhep.web.cern.ch/proj-clhep/
Vcs-Git: https://salsa.debian.org/science-team/clhep.git
Vcs-Browser: https://salsa.debian.org/science-team/clhep

Package: libclhep2t64
Provides: ${t64:Provides}
X-Time64-Compat: libclhep2.1v5
Breaks: libclhep2.1v5 (<< ${source:Version})
Replaces: libclhep2.1v5, libclhep2.1
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Class Library for High Energy Physics
 CLHEP provides a set of HEP-specific foundation and utility classes such
 as random generators, physics vectors, geometry and linear algebra.
 .
 This package provides the libraries of CLHEP.

Package: libclhep-dev
Architecture: any
Section: libdevel
Depends: libclhep2t64 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: clhep-doc
Description: Class Library for High Energy Physics - development files
 CLHEP provides a set of HEP-specific foundation and utility classes such
 as random generators, physics vectors, geometry and linear algebra.
 .
 This package provides the development files of CLHEP.

Package: clhep-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
Multi-Arch: foreign
Description: Class Library for High Energy Physics - documentation
 CLHEP provides a set of HEP-specific foundation and utility classes such
 as random generators, physics vectors, geometry and linear algebra.
 .
 This package provides the documentation of CLHEP.
