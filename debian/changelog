clhep (2.4.7.1-1) UNRELEASED; urgency=medium

  * Team upload

  [ Andreas Tille ]
  * New upstream version
  * Enable multiarch installation
  * Define SOVERSION, drop version number from library name
  * Use d-shlibs
  * Fix watch file
  * Standards-Version: 4.7.0 (routine-update)

  [ Stephan Lachnit ]
  * Fix documentation installation folder

  [ Andreas Tille ]

 -- Andreas Tille <tille@debian.org>  Thu, 09 Jan 2025 17:08:03 +0100

clhep (2.1.4.1+dfsg-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062077

 -- Steve Langasek <vorlon@debian.org>  Wed, 28 Feb 2024 16:00:23 +0000

clhep (2.1.4.1+dfsg-1.1) unstable; urgency=medium

  * Non-maintainer upload.

  [ Reiner Herrmann ]
  * getObjectList.in: Sort file list to get reproducible results
    (Closes: #794398)

  [ Vagrant Cascadian ]
  * debian/rules: Avoid embedding the build path in clhep-config.

 -- Vagrant Cascadian <vagrant@reproducible-builds.org>  Thu, 22 Dec 2022 13:06:15 -0800

clhep (2.1.4.1+dfsg-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload
  * Replace psfig by graphicx
    Closes: #820797
  * DEP5 copyright to enable using Files-Excluded: */psfig.sty

 -- Ole Streicher <olebole@debian.org>  Tue, 07 Feb 2017 12:01:31 +0100

clhep (2.1.4.1-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename library packages for g++5 ABI transition (closes: 791001).

 -- Julien Cristau <jcristau@debian.org>  Sun, 16 Aug 2015 17:33:27 +0200

clhep (2.1.4.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix broken double comparision in a testcase. (Closes: #733301)

 -- Philipp Kern <pkern@debian.org>  Fri, 25 Apr 2014 23:56:31 +0200

clhep (2.1.4.1-1) unstable; urgency=low

  * New upstream release. (Closes: #713377)
  * Bump Standards-Version to 3.9.4.
  * Remove obsolte DM-Upload-Allowed field.
  * Canonical VCS-* fields.

 -- Lifeng Sun <lifongsun@gmail.com>  Sat, 14 Dec 2013 16:33:36 +0800

clhep (2.1.3.1-1) unstable; urgency=low

  * Upload to sid.

 -- Lifeng Sun <lifongsun@gmail.com>  Mon, 06 May 2013 11:58:52 +0800

clhep (2.1.3.1-1~exp1) experimental; urgency=low

  * New upstream.

 -- Lifeng Sun <lifongsun@gmail.com>  Fri, 16 Nov 2012 14:46:08 +0800

clhep (2.1.3.0-1~exp1) experimental; urgency=low

  * New upstream.

 -- Lifeng Sun <lifongsun@gmail.com>  Sun, 11 Nov 2012 16:45:04 +0800

clhep (2.1.2.5-1~exp1) experimental; urgency=low

  * New upstream.

 -- Lifeng Sun <lifongsun@gmail.com>  Tue, 02 Oct 2012 21:06:18 +0800

clhep (2.1.2.4-1~exp1) experimental; urgency=low

  * New upstream.
  * debian/rules: change to tiny-style.

 -- Lifeng Sun <lifongsun@gmail.com>  Tue, 10 Jul 2012 16:49:02 +0800

clhep (2.1.2.3-1) unstable; urgency=low

  * New upstream.
  * Support "parallel" option of DEB_BUILD_OPTIONS.
  * debian/watch: support --destdir option of uscan. Thanks to Daniel
    Leidert <daniel.leidert@wgdd.de>

 -- Lifeng Sun <lifongsun@gmail.com>  Wed, 13 Jun 2012 20:04:32 +0800

clhep (2.1.2.2-1) unstable; urgency=low

  * Initial release (Closes: #636972)

 -- Lifeng Sun <lifongsun@gmail.com>  Sat, 19 May 2012 21:14:40 +0800
